package br.com.xtremeti.testrestbeanvalidation.quarto.entidade;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Quarto {

    @Size(min = 3, max = 10, message = "Tamanho da identificação precisa estar entre 3 e 10 caracteres")
    private String id;
    @Size(min = 5, max = 50, message = "Tamanho precisa ser entre 5 e 50")
    private String descricao;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return "Quarto{" + "id=" + id + ", descricao=" + descricao + '}';
    }

    public Quarto() {
    }

    public Quarto(String id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

}
