package br.com.xtremeti.testrestbeanvalidation.quarto.boundary;

import br.com.xtremeti.testrestbeanvalidation.quarto.entidade.Quarto;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("quartos")
public class QuartosResource {

    @Inject
    Quartos quartos;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public @Valid Quarto recuperar() {
        return new Quarto("01", "D");
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void registrar(@Valid Quarto quarto) {
        quartos.registrar(quarto);
    }
}
