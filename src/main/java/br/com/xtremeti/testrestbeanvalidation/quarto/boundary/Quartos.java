package br.com.xtremeti.testrestbeanvalidation.quarto.boundary;

import br.com.xtremeti.testrestbeanvalidation.quarto.entidade.Quarto;
import javax.ejb.Stateless;

@Stateless
public class Quartos {
    
    public void registrar(Quarto quarto){
        System.out.println("quarto registrado");
    }
}
