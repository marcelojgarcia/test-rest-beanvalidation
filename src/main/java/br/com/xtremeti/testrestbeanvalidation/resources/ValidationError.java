package br.com.xtremeti.testrestbeanvalidation.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entidade representado erros de validação.
 *
 * @see
 * {@link https://jaxenter.com/integrating-bean-validation-with-jax-rs-2-106887.html}
 * Default validation error entity to be included in {@code Response}.
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class ValidationError {

    private String invalidValue;

    private String message;

    private String messageTemplate;

    private String path;

    public ValidationError() {

    }

    public ValidationError(final String invalidValue, final String message,
            final String messageTemplate, final String path) {

        this.invalidValue = invalidValue;

        this.message = message;

        this.messageTemplate = messageTemplate;

        this.path = path;

    }

    public String getInvalidValue() {
        return invalidValue;
    }

    public void setInvalidValue(String invalidValue) {
        this.invalidValue = invalidValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageTemplate() {
        return messageTemplate;
    }

    public void setMessageTemplate(String messageTemplate) {
        this.messageTemplate = messageTemplate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
