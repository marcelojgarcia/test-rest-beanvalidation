# Exemplo de utilização de JavaBeans Validation (Bean Validation) e JAX-RS (REST)

Mapeia as exceções de validação ConstraintViolationException para geração de 
mensagens de erro mais significativas, além do código http 400/500

# Considerações

Não é possível capturar exceções de mais alto nível da que foi lançada.
Ex: Não é possível capturar ValidationException quanto ConstraintViolationException
é lançada.
